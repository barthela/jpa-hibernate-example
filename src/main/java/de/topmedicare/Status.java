package de.topmedicare;

import javax.persistence.*;

/**
 * Created by Andreas Barthel on 03.06.14.
 */

@Entity
@Table(name = "status")
@SequenceGenerator(name="status_id_seq", sequenceName = "status_id_seq", allocationSize = 1)
public class Status {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "status_id_seq")
    @Column(name = "id")
    private long id;
    @Column(name = "token")
    private char token;
    @Column(name = "description")
    private String description;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public char getToken() {
        return token;
    }

    public void setToken(char token) {
        this.token = token;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
