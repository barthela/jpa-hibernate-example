package de.topmedicare;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("de.topmedicare.JPATest");
        EntityManager em = emf.createEntityManager();
        EntityTransaction t = em.getTransaction();
        t.begin();
        Status status = new Status();
        status.setToken('X');
        status.setDescription("Xerox");

        em.persist(status);
        t.commit();

        em.close();
        emf.close();
    }
}
