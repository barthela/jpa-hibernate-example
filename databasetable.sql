--
-- PostgreSQL database dump
--
CREATE TABLE status (
    id bigint NOT NULL,
    token character(1) NOT NULL,
    description text NOT NULL
);

ALTER TABLE ONLY status
    ADD CONSTRAINT status_pkey PRIMARY KEY (id);

ALTER TABLE ONLY status
    ADD CONSTRAINT status_token_key UNIQUE (token);

CREATE SEQUENCE status_id_seq
